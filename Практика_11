import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import seaborn as sns
from sklearn.model_selection import train_test_split
from sklearn.ensemble import RandomForestRegressor
import xgboost as xgb
from sklearn.ensemble import ExtraTreesRegressor

# считывание данных
df = pd.read_csv('move.csv')

# цены в numeric
df['price'] = pd.to_numeric(df['price'], errors='coerce')

# удаление выбросов на основе метода IQR
def remove_outliers(df, column):
    q1 = df[column].quantile(0.25)
    q3 = df[column].quantile(0.75)
    iqr = q3 - q1
    df = df[(df[column] >= q1 - 1.5 * iqr) & (df[column] <= q3 + 1.5 * iqr)]
    return df

# удаление выбросов из цен
df = remove_outliers(df, 'price')

# удаление дубликатов
df = df.drop_duplicates()

# удаляем строчки с нулевыми значениями
df = df.dropna()

# разбивка данных на обучающую и тестовую выборки
X = df.drop('price', axis=1)
y = df['price']
X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.2, random_state=42)

# тренировка и оценка модели RandomForestRegressor
rf = RandomForestRegressor(n_estimators=100, random_state=42)
rf.fit(X_train, y_train)
rf_score = rf.score(X_test, y_test)

# тренировка и оценка модели XGBRegressor
xgb_reg = xgb.XGBRegressor(n_estimators=100, random_state=42)
xgb_reg.fit(X_train, y_train)
xgb_score = xgb_reg.score(X_test, y_test)

# тренировка и оценка модели ExtraTreesRegressor
et = ExtraTreesRegressor(n_estimators=100, random_state=42)
et.fit(X_train, y_train)
et_score = et.score(X_test, y_test)

# вывод результатов моделей
print('RandomForestRegressor score:', rf_score)
print('XGBRegressor score:', xgb_score)
print('ExtraTreesRegressor score:', et_score)

# вывод важности фичеров
importances = rf.feature_importances_
indices = np.argsort(importances)[::-1]
for f in range(X.shape[1]):
    print("%d. feature %d (%f)" % (f + 1, indices[f], importances[indices[f]]))

# визуалтзация моделей
plt.figure(figsize=(12, 6))
plt.subplot(1, 3, 1)
plt.scatter(rf.predict(X_test), y_test)
plt.xlabel('Predicted Price')
plt.ylabel('Actual Price')
plt.title('RandomForestRegressor')

plt.subplot(1, 3, 2)
plt.scatter(xgb_reg.predict(X_test), y_test)
plt.xlabel('Predicted Price')
plt.ylabel('Actual Price')
plt.title('XGBRegressor')

plt.subplot(1, 3, 3)
plt.scatter(et.predict(X_test), y_test)
plt.xlabel('Predicted Price')
plt.ylabel('Actual Price')
plt.title('ExtraTreesRegressor')

plt.tight_layout()
plt.show()